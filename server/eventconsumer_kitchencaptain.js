var amqp = require('amqplib');

amqp.connect('amqp://192.168.99.100').then(function(conn) {
  process.once('SIGINT', function() { conn.close(); });
  return conn.createChannel().then(function(ch) {

    var ok = ch.assertQueue('KITCHEN_CAPTAIN', {durable: true});

    ok = ok.then(function(_qok) {
		
      return ch.consume('KITCHEN_CAPTAIN', function(msg) {
		  
        console.log(" [x] Received '%s'", msg.content.toString());
		
      }, {noAck: true});
	  
    });

    return ok.then(function(_consumeOk) {
		
      console.log(' [Kitchen captain] Waiting for messages . To exit press CTRL+C');
	  
    });
  });
}).catch(console.warn);