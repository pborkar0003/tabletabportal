"use strict";
class Order {

    constructor (db) {
		
        this.database = db;
		this.collectionName = "order";
	}
	
	//add order to the database
	addOrder(req, res, next){
		
		var self = this;
		var order = req.body.order;
		console.log("order: ",typeof order)
		var order = JSON.parse(order);
		order.creation_date =  Date.now();
		
		
		self.database.saveObject(self.collectionName,order,function(message,status){
			
			if(status){
				
				res.status(200).json(message);
			}else{
			
				res.status(500).json("Server error!! please try after some time");
			}
			
		})
	}
	
	//return orders to the caller
	getOrders(req, res, next){
	
		var self = this;
		
		var fields ={
		}
		self.database.getObjects(self.collectionName,fields,function(err,results){
			
			if(err){
				console.log(err);
				res.status(500).json("Server error!! please try after some time");
			}else{
				res.status(200).json(results);
			}
		})
	}
	
	//update order with specified order id in the table;
	//requires 2 args, orderid,itemdata
	updateOrder(req, res, next){
	
		var self = this;
		var newItem = true;
		var orderId = req.body.orderid
		var oldObj = {
			_id: new ObjectID(orderId)
		}
		var item = req.body.item
		
		self.database.getObject(self.collectionName,{_id:orderId},function(orderItem){
				orderItem = orderItem[0]
				console.log("order details: ",orderItem);
				if(orderItem.type="FOOD_ORDER"){
					
					var details = orderItem.details;
					for(var i=0;i<details.length;i++){
						
						var foodOrderItem = details[i];
						if(foodOrderItem.orderid == item.orderid){
							
							newItem = false;
							details[i] = item;
							break;
						}
					}
					if(newItem){
						details.push(item)
					}
					orderItem.details =  details
					
				}else{
					
					orderItem.details[0] = item
				}
				console.log("updated order details: ",orderItem);
				delete orderItem._id;
				
				self.database.updateObject(self.collectionName,oldObj,orderItem,function(err,results){
			
					if(err){
						console.log(err);
						res.status(400).json("Updation failed!! please try again");
					}else{
						res.status(200).json("Order updated sucessfully");
					}
				})
		});
	}

	//delete the order from the table;
	//arguments required is 2; one for orderid,other for itemid; if itemid is null delete entire order
	deleteOrder(req,res,next){
	
		var self=this;
		var orderId = req.params.id
		var oldObj = {
			_id:new ObjectID(orderId)
		}
		var itemid = req.params.item
		if(itemid){			
			self.database.getObject(self.collectionName,{_id:orderId},function(orderItem){
				
				console.log("order details: ",orderItem);
				if(orderItem.type="FOOD_ORDER"){
					
					var details = orderItem.details;
					for(var i=0;i<details.length;i++){
						
						var foodOrderItem = details[i];
						if(foodOrderItem.orderid == itemid){
							
							details.splice(i,1)
							break;
						}
					}
					orderItem.details =  details
					console.log("updated order details: ",orderItem);
				
					self.database.updateObject(self.collectionName,oldObj,orderItem,function(err,results){
				
						if(err){
							console.log(err);
							res.status(400).json("Updation failed!! please try again");
						}else{
							res.status(200).json("Order deleted sucessfully");
						}
					})
					
				}
				
			});
			
		}else{
			
			self.database.deleteObject(self.collectionName,{_id:orderId},function(err,results){
			
				if(err){
					console.log(err);
					res.status(400).json("Deletion failed!! please try again");
				}else{
					res.status(200).json("Order deleted sucessfully");
				}
			
			});
			
		}
		
	}

	
	ackOrder(req,res,next){
		
		var self = this;
		var orderId = req.body.orderid
		var oldObj = {
			_id: new ObjectID(orderId)
		}
		var ackDate = Date.now();
		self.database.getObject(self.collectionName,oldObj,function(orderItem){
				orderItem = orderItem[0]
				delete orderItem._id;
				orderItem.acknowledged_date = ackDate;
				
				self.database.updateObject(self.collectionName,oldObj,orderItem,function(err,results){
			
					if(err){
						console.log(err);
						res.status(400).json("Updation failed!! please try again");
					}else{
						res.status(200).json(ackDate);
					}
				})
			
		});
		
	}
	
	cancelOrder(req,res,next){
		
		var self = this;
		var orderId = req.body.orderid
		var oldObj = {
			_id: new ObjectID(orderId)
		}
		var calcelDate = Date.now();
		self.database.getObject(self.collectionName,oldObj,function(orderItem){
				orderItem = orderItem[0]
				delete orderItem._id;
				orderItem.cancel_date = calcelDate;
				
				self.database.updateObject(self.collectionName,oldObj,orderItem,function(err,results){
			
					if(err){
						console.log(err);
						res.status(400).json("Updation failed!! please try again");
					}else{
						res.status(200).json(calcelDate);
					}
				})
			
		});
		
		
	}
	
	completeOrder(req,res,next){
		
		var self = this;
		var orderId = req.body.orderid
		var oldObj = {
			_id:orderId
		}
		var completeDate = Date.now();
		self.database.getObject(self.collectionName,oldObj,function(orderItem){
				orderItem = orderItem[0]
				delete orderItem._id;
				orderItem.cancel_date = completeDate;
				
				self.database.updateObject(self.collectionName,oldObj,orderItem,function(err,results){
			
					if(err){
						console.log(err);
						res.status(400).json("Updation failed!! please try again");
					}else{
						res.status(200).json(completeDate);
					}
				})
			
		});
		
		
		
	}

	getOrdersByRoom(req,res,next){
		
		var self = this;
		var roomno = req.params.roomid
		var checkindate = req.params.checkin
		var checkoutdate = req.params.checkout
		
		var obj ={
			'roomno':roomno,
			'creation_date':{ $gte: parseInt(checkindate),
							  $lte: parseInt(checkoutdate) 
						}
		}
		var condition = {
			
		}
		self.database.getObjectOnCondition(self.collectionName,obj,condition,function(err,results){
			
			if(err){
				console.log(err);
				res.status(500).json("Server error!! please try after some time");
			}else{
				res.status(200).json(results);
			}
		})
		
	}
}
module.exports = Order;
