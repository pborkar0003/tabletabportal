"use strict";
class MQConnector {

	
    constructor (host,port) {
		var self  =this;
		amqp.connect(host).then(function(conn) {
			
			conn.createChannel().then(function(ch) {
				
				self.conn = conn;
				self.ch = ch;
			});
			
		});
	
	}
	
	sendMessage (queue,message,callback){

		var ok = this.ch.assertQueue(queue, {durable: true});
		ok.then(function(_qok){
			  var issent = ch.sendToQueue(q, Buffer.from(JSON.stringify(message)));
			  callback(issent)
		});
		
	}
	closeConnection(){
		
		this.conn.close();
		this.ch.close();
		
	}
	
}
module.exports = MQConnector;
