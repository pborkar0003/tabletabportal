var amqp = require('amqplib');

amqp.connect('amqp://192.168.99.100').then(function(conn) {
  
  return conn.createChannel().then(function(ch) {
		
		var q = 'tab1';
		var orderData = {
		
			roomno:"room_002",
			type:"ROOM_SERVICE",
			intended_user:"RECEPTIONIST",
			details:["Need to change bedsheet"]
		}
		var ok = ch.assertQueue(q, {durable: true});

		return ok.then(function(_qok) {
		  ch.sendToQueue(q, Buffer.from(JSON.stringify(orderData)));
		  return ch.close();
		});
	
  }).finally(function() { conn.close(); });
  
}).catch(console.warn);