tabletTab.controller('roomsummaryController', ['$scope', '$rootScope','$http','$cookieStore','$location',
    function ($scope, $rootScope,$http,$cookieStore,$location) {
    	 
	console.log("roomsummaryController");
	var tempavailablerooms = [];
	var token = $cookieStore.get('token')
	if(token==null){
		$location.path("\homepage")
	}
	$scope.checkfields={};
	$scope.filter={};
	$scope.availablerooms = [];

	fetchRoomSummary();
	
	$scope.refresh = function(){
		
		//console.log("refreshed clicked")
		fetchRoomSummary();
	}
	
	$scope.clearResponse = function(){
		
		$scope.availability_response = '';
		if(tempavailablerooms.length > 0){
			$scope.availablerooms = tempavailablerooms
		}
	}
	$scope.clear = function(){
		$scope.availability_response = '';
		$scope.checkfields = {};
		if(tempavailablerooms.length > 0){
			$scope.availablerooms = tempavailablerooms
		}
	}
	
	function fetchRoomSummary(){
		
		var header = getHeader();
					
		$http.get(APIURL+'/room/summary',{headers:header}).then((response)=>{

			console.log(response)
			$scope.availablerooms =response.data
			
		},(err)=>{
			
			console.log(err)
		});	
		
	}
	function checkItem(item,checkfields){

		if(checkfields.roomtype){
			if(item.type!=checkfields.roomtype){
				return false;
			}
		}
		if(checkfields.roomclass){
			if(checkfields.roomclass!=item.class){
				return false;
			}
		}
		if(checkfields.checkin && checkfields.checkout){

			var temp_checkin = new Date(checkfields.checkin).getTime()
			var temp_checkout = new Date(checkfields.checkout).getTime()
			for(var i=0;i<item.output.length;i++){
					
				var bookedstats = item.output[i];
				if(temp_checkin<=bookedstats.checkout && temp_checkout >= bookedstats.checkin){
						
					return false;
				}
			}
		}
		return true;
		
		
	}
	$scope.checkAvailibility = function(checkfields){
		
		var avalRooms = [];
		var returnflag;
		tempavailablerooms = angular.copy($scope.availablerooms);
		$scope.availablerooms.forEach(function(item) {
			
			if(checkItem(item,checkfields)){
				avalRooms.push(item)
			}
			
		});
		if(avalRooms.length > 0){
			
			$scope.availability_response = avalRooms.length+ " room(s) available!!!";
			$scope.availablerooms = avalRooms;
			
		}else{
			
			$scope.availability_response = "No rooms available!!!";
		}
		
		
	}

	function getHeader(){
		var header = {
			"Authorization":"JWT "+token
		}
		return header;
	}

	$scope.getStatus = function(room){
		
		
		var output = room.output
		var todaysDate = new Date().getTime();
		for(var i=0;i< room.output.length;i++){
			
			var booked_item = room.output[i];
			if(booked_item.checkin<=todaysDate && booked_item.checkout>=todaysDate){
				
				return true
			}
				
			
		}
	
		return false;
		
	}
	$scope.getBookedDate = function(room){
		
		
		var output = room.output
		var todaysDate = new Date().getTime();
		for(var i=0;i< room.output.length;i++){
			
			var booked_item = room.output[i];
			if(booked_item.checkin<=todaysDate && booked_item.checkout>=todaysDate){
				
				return booked_item.checkout
			}
				
			
		}
		return "---";
		
	}
}]);
