tabletTab.controller('homeController', ['$scope', '$rootScope','$http','$cookieStore','$location',
    function ($scope, $rootScope,$http,$cookieStore,$location) {
    	 
	$scope.Math = window.Math;
	$scope.showOrder="";
	console.log("homecontroller: ",$scope.user.role);
	var token = $cookieStore.get('token')
	if(token==null){
		$location.path("\homepage")
	}
    $scope.allOrderList={}
	
	fetchAllOrders();
	
	$scope.refresh = function(){
		
		console.log("refreshed clicked")
		fetchAllOrders();
		
	}
	$scope.setShowOrder = function(order){
		
		if(order._id!==$scope.showOrder){
			$scope.showOrder=order._id;
		}else{
			$scope.showOrder="";
		}
		
		
	}
	$scope.ackOrder = function(order){
		var header = getHeader();
		$http.put(APIURL+'/order/ack',{orderid:order._id},{headers:header}).then((response)=>{
			
				
			$scope.allOrderList[order._id].acknowledged_date = response.data;
			
			
		},(err)=>{
			 
			 console.log('error while ack order: ',err)
		})
	}
	
	$scope.cancelOrder = function(order){
		
		var header = getHeader();
		$http.put(APIURL+'/order/cancel',{orderid:order._id},{headers:header}).then((response)=>{
			
				
			$scope.allOrderList[order._id].cancel_date = response.data;
			
			
		},(err)=>{
			 
			 console.log('error while cancel order: ',err)
		})
	}
	
	
	
	$scope.toDay = function(history){

		console.log("history: ",history)
		history.temp_date = new Date(history.creation_date).toDateString();
		return history;
	}
	
	function fetchAllOrders(){
		
		var header = getHeader();
		
		$http.get(APIURL+'/order',{headers:header}).then((response)=>{
			
				console.log("response: ",response.data)
				for(var i=0;i<response.data.length;i++){
					
					var data = response.data[i];
					$scope.allOrderList[data._id] = data;
					
				}
				console.log($scope.allOrderList)
				
			},(err)=>{
				
				console.log(err)
		});
		
	}
	
	function getHeader(){
		var header = {
			"Authorization":"JWT "+token
		}
		return header;
	}
	
}]);